# ZXDIY小程序开源版

#### 介绍
国内首个完全开源——基于小程序可视化DIY操作生成微信小程序，包括零售小程序、轻站小程序。基于小程序云开发实现，无需单独部署服务器，无论是个人还是公司，该款小程序非你莫属。

#### 软件架构
软件架构说明:微信小程序+云开发 


#### 安装教程

1. 通过微信开发者工具导入源码（需要系统安装nodejs环境）
2. 点击云开发，新建数据环境名称product，将代码中product-6d4b5e修改为您新建的环境名称，提交云开发代码
3. 然后新建数据集合，名称包括：coupon_info、index_diy、order_ana、order_info、pictures、shop_product、user_coupon、user_info、user_list
4. 编译、预览即可体验效果

#### DIY使用说明

1. 支持自由面板、文本、图片、菜单、搜索、轮播、商品、通告栏组件
2. 小程序中直接体验DIY操作，点击预览即可体验效果

#### 体验

如需体验小程序，请扫码申请体验权限

![](/pleaseDelete/user.jpg)

#### 问题反馈

在使用中有任何问题，请使用以下联系方式联系我们
QQ群: 564126273(交流群①)

![](/pleaseDelete/s11.png)


### 项目截图

| ![1](/pleaseDelete/s1.png) | ![2](/pleaseDelete/s2.png) | ![3](/pleaseDelete/s10.png) | ![4](/pleaseDelete/s4.png) |
| --------------------------- | --------------------------- | --------------------------- | --------------------------- |
| ![5](/pleaseDelete/s5.png) | ![6](/pleaseDelete/s6.png) | ![7](/pleaseDelete/s7.png) | ![8](/pleaseDelete/s8.png) |
